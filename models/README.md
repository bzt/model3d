Model 3D Samples
================

NOTE: these are just for API testing purposes. You can find a lot more model samples on the [Model 3D website](https://bztsrc.gitlab.io/model3d/#models).

- aliveai_character.m3d - from Minetest aliveai mod (textures, animations, original 47k, m3d 2.5k)
- bezier.a3d, bezier.m3d - examples with Bezier curves
- cube.m3d - smallest possible example, 119 bytes only
- cube_normals.m3d - cube with normal vectors, 159 bytes
- cube_usemtl.m3d - converted from Assimp sample OBJ by the same name, cube with materials
- cube_with_vertexcolors.m3d - converted from Assimp sample OBJ by the same name, cube with vertex colors
- cube_with_vertexcolors.a3d - same, but saved in ASCII variant with Windows line endings (\r\n)
- lantea.m3d - more complex voxel model, converted from Minecraft Schematics (colors and voxel names)
- mobs_dwarves_character.m3d - from Minetest mobs_dwarves mod (original 151k, m3d 8.5k)
- monu1.m3d - exported from Goxel (voxel image, original vox 628k, m3d 2.4k)
- nurbs.a3d, nurbs.m3d - examples of a textured non-uniform rational B-spline surface
- suzanne.m3d - exported from Blender (monkey face, with normals and texture UVs and materials)
- CesiumMan.m3d - exported from Blender (rigged, animated skeleton, [original](https://github.com/KhronosGroup/glTF-Sample-Models/tree/master/2.0/CesiumMan/glTF) 250k, m3d only 73k)
- voxel.a3d, voxel.m3d - sime voxel image, converted from Magicavoxel VOX (colors only)
- WusonBlitz0.m3d - from Assimp sample by the same name (was 87k, triangle mesh) with int8 coordinates, minor quality degradation
- WusonBlitz1.m3d - same, but uses int16 coordinates (no noticable difference to the original, but just 35k)
- WusonBlitz2.m3d - same, but with 32 bit floating point numbers (same precision as the original, half the file size, 42k)
